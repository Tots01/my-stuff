import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ms-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() text: string;

  @Output() click = new EventEmitter();
  @Output() mouseover = new EventEmitter();
  @Output() mouseout = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  onClicked() {
    this.click.emit();
  }

  onMouseOver() {
    this.mouseover.emit();
  }

  onMouseOut() {
    this.mouseout.emit();
  }
}
